package main;

/**
 * Utilities
 * 
 * @author DSolanas
 * @version 0.1
 */

public class Util {
	
	/**
	 * Given an array of values and the value to be scalated, it scalates to [0-1]
	 * 
	 * return = (valueToScalate-min)/(max-min)
	 * @return
	 */
	public static double scalate(double valueToScalate, double min, double max) {
		return ((valueToScalate - min) / (max - min));
	}
	
	/**
	 * Given an array of values and the value to be de-scalated, it de scalates from [0-1]
	 * 
	 * return = ((max-min)*scalated) + min
	 * @return
	 */
	public static double deScalate(double valueToDeScalate, double min, double max) {
		return ((max - min) * valueToDeScalate) + min;
	}
	
	public static double getMin(double[] values) {
		double min = Double.MAX_VALUE;
		
		for (double v : values) {
			if (v < min) {
				min = v;
			}
		}
		
		return min;
	}
	
	public static double getMax(double[] values) {
		double max = Double.MIN_VALUE;
		
		for (double v : values) {
			if (v > max) {
				max = v;
			}
		}
		
		return max;
	}
	
	public static double summation (double[] elements) {
		double result = 0;
		
		for (double d : elements) {
			result += d;
		}
		
		return result;
	}
}
