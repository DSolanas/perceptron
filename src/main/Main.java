package main;

import network.Network;

/**
 * Main class to start and test neural network.
 * 
 * In the future this should dissapear and the project should become a library to use wherever the user wants.
 * 
 * The references are below.
 * 
 * @author DSolanas
 * @version 0.1
 * @see https://www.youtube.com/playlist?list=PLAnA8FVrBl8AWkZmbswwWiF8a_52dQ3JQ
 */

public class Main {

	public static void main(String[] args) {
		new Network().start(); 
	}

}
