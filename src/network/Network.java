package network;

import java.util.ArrayList;
import java.util.List;

import entities.Connection;
import entities.Neuron;
import main.Util;

/**
 * Main place, where everything is going to be done
 * 
 * @author DSolanas
 * @version 0.1
 */

public class Network {
	
	// Configuration constants
	private static final double LEARNING_FACTOR = 0.01;	// Learning factor. Big, faster inaccurate learning. Small slower accurate learning.
	
	// Input and output
	private double[] input;
	private double[] output;
	
	// Network structure
	private int layers;				// Layers that will have the network. Including the first and the last one
	private int neuronsPerLayer;	// Neurons that will have every layer except the first and the last one
	
	private List<List<Neuron>> networkMap;	// Layers(Neurons)
	

	public Network() {
		System.out.println("NETWORK -- Layers: " + layers + " -- Neurons per layer: " + neuronsPerLayer);
		buildNetwork();
	}
	
	public void start() {
		// First of all, we escalate the input values between [0-1]
		scalateInputValues();
		
		processValues();
		
		// Before finishing, we de-escalate the output values to the original values
		deScalateOutputValues();
	}
	
	private void buildNetwork() {
		System.out.println("Building network");
		
		// MAP
		buildMap();
		
		// CONNECTIONS
		buildConnections();
		
	}
	
	private void processValues() {
		for (int i = 0; i < networkMap.size() - 1; i++) {	// Each layer except the last one
			List<Neuron> layer = networkMap.get(i);
			
			for (int j = 0; j < layer.size(); j++) {	// Each neuron
				Neuron neuron = layer.get(j);
				
				neuron.passValueToConnections();
			}
		}
		
		// LAST LAYER
		List<Neuron> layer = networkMap.get(networkMap.size() - 1);	// Last layer
		for (int i = 0; i < layer.size(); i++) {	// Each neuron in the last layer
			Neuron neuron = layer.get(i);
			output[i] = neuron.getValueToPass();	// The output
		}
	}
	
	private void buildMap() {
		System.out.println("Building network -- Building map");
		
		for (int i = 0; i < layers; i++) {	// We create the neurons and give the input ones its values
			networkMap.add(new ArrayList<Neuron>());
			
			if (i == 0) {	// If we are on the input layer
				for (int j = 0; i < input.length; j++) {
					Neuron neuron = new Neuron();
					neuron.setValue(input[j]);
					networkMap.get(i).add(neuron);
				}
			} else if (i == layers - 1) {	// If we are on the output layer
				for (int j = 0; j < output.length; j++) {
					Neuron neuron = new Neuron();
					networkMap.get(i).add(neuron);
				}
			} else {	// If we are on the hidden layers
				for (int j = 0; j < neuronsPerLayer; j++) {
					Neuron neuron = new Neuron();
					networkMap.get(i).add(neuron);
				}
			}
		}
	}
	
	private void buildConnections() {
		System.out.println("Building network -- Building connections");
		
		if (networkMap == null || networkMap.isEmpty()) {
			System.err.println("ERROR -- Network -- buildConnections() -- networkMap is not built, connections cannot be built.");
			return;
		}
		
		for (int i = 0; i < networkMap.size(); i++) {	// We make the connections here.
			List<Neuron> previousLayer;	// The previous layer only when it is needed
			List<Neuron> layer = networkMap.get(i);	// We take each layer in the network
			List<Neuron> nextLayer;	// The next layer only when it is needed
			
			for (int j = 0; j < layer.size(); j++) {
				Neuron neuron = layer.get(j);	// We take each neuron in the layer
				
				if (i  == 0) {	// We are on the input layer. (Only nextNeurons will be needed)
					nextLayer = networkMap.get(i + 1);
					
					addNextConnections(nextLayer, neuron);
				} else if (i == layers - 1) {	// We are on the output layer. (Only previousNeurons will be needed)
					previousLayer = networkMap.get(i - 1);
					
					addPreviousConnections(previousLayer, neuron);
				} else {	// We are on a hidden layer. (Both direction connections are needed)
					nextLayer = networkMap.get(i + 1);
					previousLayer = networkMap.get(i - 1);
					
					addNextConnections(nextLayer, neuron);
					addPreviousConnections(previousLayer, neuron);
				}
			}
		}
	}

	private void scalateInputValues() {
		
		double min = Util.getMin(input);
		double max = Util.getMax(input);
		
		for (double v : input) {
			Util.scalate(v, min, max);
		}
	}
	
	private void deScalateOutputValues() {
		double min = Util.getMin(output);
		double max = Util.getMax(output);
		
		for (double v : output) {
			Util.deScalate(v, min, max);
		}
	}
	
	private void addPreviousConnections (List<Neuron> previousLayer, Neuron neuron) {
		// This neuron will have the number of neurons in the previous layer as amount of previous connections
		Connection[] previousConnections = new Connection[previousLayer.size()];
		for (int k = 0; k < previousLayer.size(); k++) {
			previousConnections[k].setPreviousNeuron(previousLayer.get(k));	// Adds each time a neuron from the previous layer
			previousConnections[k].setNextNeuron(neuron);
		}
		neuron.setPreviousConnections(previousConnections);
	}

	private void addNextConnections (List<Neuron> nextLayer, Neuron neuron) {
		// This neuron will have the number of neurons in the next layer as amount of forward connections
		Connection[] nextConnections = new Connection[nextLayer.size()];
		for (int k = 0; k < nextLayer.size(); k++) {
			nextConnections[k].setPreviousNeuron(neuron);
			nextConnections[k].setNextNeuron(nextLayer.get(k));	// Adds each time a neuron from the next layer
		}
		neuron.setNextConnections(nextConnections);
	}

	public static double getLearningFactor() {
		return LEARNING_FACTOR;
	}

	public double[] getInput() {
		return input;
	}

	public void setInput(double[] input) {
		this.input = input;
	}

	public double[] getOutput() {
		return output;
	}

	public void setOutput(double[] output) {
		this.output = output;
	}

	public int getLayers() {
		return layers;
	}

	public void setLayers(int layers) {
		this.layers = layers;
	}

	public int getNeuronsPerLayer() {
		return neuronsPerLayer;
	}

	public void setNeuronsPerLayer(int neuronsPerLayer) {
		this.neuronsPerLayer = neuronsPerLayer;
	}
}
