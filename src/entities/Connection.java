package entities;

/**
 * Connection component of the neural network
 * 
 * @author DSolanas
 * @version 0.1
 */
public class Connection {
	
	private static final double INITIAL_WEIGHT = 1;	// The initial weight, if no other is defined
	
	private Neuron previousNeuron;	// The previous neuron in the network null if it's one of the first neurons
	private Neuron nextNeuron;		// The next neurons in the network null if it's one of the last neurons
	
	private double weight = INITIAL_WEIGHT;	// Importance of the connection to the network (w).
	private double value;	// The value to pass to the next neuron
	
	public Connection() {
		this (null, null, -1);	// Calls to the complete constructor
	}

	public Connection(Neuron previousNeuron, Neuron nextNeuron) {
		this (previousNeuron, nextNeuron, -1);
	}

	public Connection(Neuron previousNeuron, Neuron nextNeuron, double weight) {
		this.previousNeuron = previousNeuron;
		this.nextNeuron = nextNeuron;
		this.weight = weight;
	}

	public Connection(Neuron previousNeuron, Neuron nextNeuron, double weight, double value) {
		this.previousNeuron = previousNeuron;
		this.nextNeuron = nextNeuron;
		this.weight = weight;
		this.value = value;
	}

	public Neuron getPreviousNeuron() {
		return previousNeuron;
	}

	public void setPreviousNeuron(Neuron previousNeuron) {
		this.previousNeuron = previousNeuron;
	}

	public Neuron getNextNeuron() {
		return nextNeuron;
	}

	public void setNextNeuron(Neuron nextNeuron) {
		this.nextNeuron = nextNeuron;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public static double getInitialWeight() {
		return INITIAL_WEIGHT;
	}

	public double getValueToPass() {
		return previousNeuron.getValue() * weight;
	}
	
	/**
	 * 
	 * @param value The value to be pass to the next neuron
	 * @return Different values depending on the result:
	 * 				- 0 If everything went ok.
	 * 				- 1 If there is no next neuron (it's the last neuron)
	 * 				- 2 If there is no previous neuron (it's the first neuron)
	 * 				- -1 If there is no previous nor next neuron (not connected)
	 */
//	public int passValueToNextNeuron(double previousValue) {
//		int result = -1;
//		double value = previousValue * weight;	// The value to be passed
//		
//		if (nextNeuron != null && previousNeuron != null) {
//			// If there is next and previous neuron we are in a hidden (intermediate) neuron
//			nextNeuron.setValue(value);
//			result = 0;
//		} else if (nextNeuron != null && previousNeuron == null) {
//			// If there is no next neuron we are in the last neuron
//			result = 1;
//		} else if (nextNeuron == null && previousNeuron != null) {
//			// If there is no previous neuron we are in the first neuron
//			result = 2;
//		}
//		
//		return result;
//	}
}
