package entities;

import main.Util;

/**
 * Neuron component of the neural network.
 * 
 * @author DSolanas
 * @version 0.1
 */

public class Neuron {
	
	private static final double INITIAL_THRESHOLD = 0.5;	// The initial threshold, if no other is defined
	
	// Relation with other neurons.
	private Connection[] previousConnections;	// The connection with the next neurons. null if it's one of the first neurons and it has not connections
	private Connection[] nextConnections;		// The connection with the previous neurons. null if it's one of the last neurons and it has no connections
//	private int layer;	// Layer where this neuron is located. 1...K . 1 is for the entrance layer and K is for the exit layer.
//	private int row;	// Position in the layer. 1 ... n . 1 is for the upper neuron and n is for the last neuron in the layer.
	
	// Properties of the neuron
	private double threshold = INITIAL_THRESHOLD;	// Importance of the neuron itself. It should be between -1 and 1;
	private boolean isRealNeuron;	// Defines if the neuron is real or not (if the neuron should be considered to get the error)
	private double value;	// Defines the value that this neuron is going to pass to the next neuron.
	
//	private double entryValue;	// Value that comes from the previous neuron or the entry got from the input system if it's the first. It should be between 0 and 1.
//	private double outputValue;	// Value that will be given to the next neuron or the output that will be given to the system if it's the last. It should be between 0 and 1.
	
	public Neuron() {
	}
	
	public Neuron(double threshold, boolean isRealNeuron) {
		// Calls to the complete constructor with default values
		this.threshold = threshold;
		this.isRealNeuron = isRealNeuron;
	}

	public Neuron(Connection[] previousConnections, Connection[] nextConnections) {
		// Calls to the complete constructor with default values
		this.previousConnections = previousConnections;
		this.nextConnections = nextConnections;
	}

	public Neuron(Connection[] previousConnections, Connection[] nextConnections, double threshold,
			boolean isRealNeuron) {
		// Calls to the complete constructor with default values
		this.previousConnections = previousConnections;
		this.nextConnections = nextConnections;
		this.threshold = threshold;
		this.isRealNeuron = isRealNeuron;
	}

	public Neuron(Connection[] previousConnections, Connection[] nextConnections, double threshold,
			boolean isRealNeuron, double value) {
		this.previousConnections = previousConnections;
		this.nextConnections = nextConnections;
		this.threshold = threshold;
		this.isRealNeuron = isRealNeuron;
		this.value = value;
	}

	public Connection[] getPreviousConnections() {
		return previousConnections;
	}

	public void setPreviousConnections(Connection[] previousConnections) {
		this.previousConnections = previousConnections;
	}

	public Connection[] getNextConnections() {
		return nextConnections;
	}

	public void setNextConnections(Connection[] nextConnections) {
		this.nextConnections = nextConnections;
	}

//	public int getLayer() {
//		return layer;
//	}
//
//	public void setLayer(int layer) {
//		this.layer = layer;
//	}
//
//	public int getRow() {
//		return row;
//	}
//
//	public void setRow(int row) {
//		this.row = row;
//	}

	public double getThreshold() {
		return threshold;
	}

	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

	public boolean isRealNeuron() {
		return isRealNeuron;
	}

	public void setRealNeuron(boolean isRealNeuron) {
		this.isRealNeuron = isRealNeuron;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public static double getInitialThreshold() {
		return INITIAL_THRESHOLD;
	}
	
	/**
	 * If is real neuron (not the first):
	 * 		threshold + SUM(getValueToPassFromThePrevious * valueToPassFromThePreviousConnection)
	 */
	public void passValueToConnections() {
		
		if (previousConnections != null && previousConnections.length > 0) {
			setRealNeuron(true);
		} else {
			setRealNeuron(false);
		}
		
		double valueToPass = getValueToPass();
		
		for (Connection c : nextConnections) {
			c.setValue(valueToPass);
		}
	}
	
	public double getValueToPass() {
		double valueToPass;
		
		double[] previousConnectionsValues = new double[previousConnections.length];
		
		for (int i = 0; i < previousConnectionsValues.length; i++) {
			previousConnectionsValues[i] = previousConnections[i].getValueToPass();
		}
		
		// If it's a hidden neuron or a final neuron it will be real. If it's real the threshold has to take effect.
		if (isRealNeuron) {
			valueToPass = threshold + (Util.summation(previousConnectionsValues));
			this.setValue(valueToPass);
		} else {
			valueToPass = value;
		}
		
		return valueToPass;
	}
}
